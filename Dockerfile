# Use a slim Python base image
FROM python:3.9-slim

# Set working directory for the application
WORKDIR /app

# Copy requirements.txt and application code
COPY requirements.txt requirements.txt
COPY . .

# Install dependencies
RUN pip install -r requirements.txt gunicorn

# Run the app with gunicorn
CMD ["sh", "-c", "gunicorn --bind :$PORT wsgi"]
